﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionManager : MonoBehaviour
{
    public GameObject point;
    public enum Destruction
    {
        Circle,
        Wall,
        Valley
    }
    public Destruction DestructionType { get; set; }
    public TerrainManager terrainManager;

    private int _minimumLevel = 10;
    bool IsPointInsideCircle(Vector2 point, Vector2 center, float radius)
    {
        return Vector2.Distance(point, center) <= radius;
    }
    public void Exp()
    {
        //BulletProperties bp = new BulletProperties();
        //bp.destructionType = Destruction.Circle;
        //bp.destructionRange = 5;


        explode((int)(terrainManager.camera.WorldToScreenPoint(point.transform.position).x), (int)(terrainManager.camera.WorldToScreenPoint(point.transform.position).y));

    }
    public int Range;
    public void explode(int refX, int refY)//, BulletProperties prop)
    {

        print("point" + terrainManager.GetScreenPosition(point.transform.position));
        //        point.transform.position = terrainManager.camera.ScreenToWorldPoint(new Vector3(refX, refY, 0));
        int range = Range; //prop.destructionRange;
        Destruction type = Destruction.Circle;//prop.destructionType;

        Vector3 vertexPos;
        int leftX = (int)Mathf.Max((float)refX - (float)range, 0.0f);

        //FIXME: Screen.width may not be the right variable
        int rightX = Mathf.Min(refX + range, Screen.width);
        print(leftX + ">>>" + rightX + ">>>" + terrainManager._STEP);
        for (int tempX = leftX; tempX < rightX; tempX += terrainManager._STEP)
        {
            vertexPos = terrainManager.upOnSurface(tempX);
            print("tmp" + tempX);
            print("SurfacePoint" + vertexPos);
            if (IsPointInsideCircle(new Vector2(vertexPos.x, vertexPos.y), new Vector2(refX, refY), range))
            {
                int resY;

                switch (type)
                {
                    case Destruction.Circle:
                        resY = weaponFuncCircle(tempX, range, refX, refY);
                        break;
                    case Destruction.Wall:
                        resY = weaponFuncWall(refY);
                        break;
                    case Destruction.Valley:
                        resY = weaponFuncValley(tempX, 1, leftX, rightX, refX, refY);
                        break;
                    default:
                        resY = weaponFuncCircle(tempX, range, refX, refY);

                        print("WARNNING ON EXPLOSION ::: DESTRUCTION TYPE NOT DEFINED. CIRCLE DESTRUCTION IS APPLIED.");
                        break;
                }
                terrainManager.VertexHeights[(int)Mathf.Round(tempX / terrainManager._STEP)] = new Vector2(vertexPos.x, Mathf.Max(resY, _minimumLevel));

            }
            else if (vertexPos.y > refY + range)
            {
                int tDist = (int)Mathf.Abs(vertexPos.x - refX);
                int tY = (int)vertexPos.y - (-tDist + range);
                terrainManager.VertexHeights[(int)Mathf.Round(tempX / terrainManager._STEP)] = new Vector2(vertexPos.x, Mathf.Max(tY, _minimumLevel));
                print("tDist" + tDist);
            }
        }
        // terrainManager.verts[3] = terrainManager.verts[3] - 5 * Vector2.one;
        terrainManager.GenerateTerrain(terrainManager.VertexHeights);

        //fixTanksToSurface();
    }


    private int weaponFuncCircle(int x, int radius, int x0, int y0)
    {

        int y = MyUtils.makeRound(-MyUtils.makeRound(Mathf.Sqrt(Mathf.Abs(MyUtils.makeRound(radius * radius) - MyUtils.makeRound((x - x0) * (x - x0)))))) + y0;
        print("CircleDestruction" + y);
        return y;
    }
    private int weaponFuncLinear(int x, float m, int x0, int y0)
    {
        int y = MyUtils.makeRound(m * (x - x0)) + y0;
        return y;
    }

    private int weaponFuncWall(int y0, int height = 130)
    {
        int y = y0 + height;
        print("Wall" + y);
        return y;
    }

    private int weaponFuncValley(int x, float m, int x1, int x2, int x0, int y0)
    {
        int y = y0;
        if (x <= x0)
        {
            y = -MyUtils.makeRound(m * (x - x1)) + y0;
        }
        else
        {
            y = -MyUtils.makeRound(-m * (x - x2)) + y0;
        }
        print("Valley" + y);
        return y;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    private int x;
    private int y;
    private bool _moving;
    private float _moveStep = 5;
    public int offsetFromSurface = 6;
    private float _moveAngle;
    private float _tractionAngle;
    private Vector2 _nextTargetPos;
    private List<Vector2> _targetPoints;
    public GameObject tankGameObject;
    public Transform endPoint;
    public Transform startPoint;
    public TerrainManager terrainManager;
    public AnimationCurve changeCurve;
    public GameObject aim;
   public GameObject gun;
    public Bullet bulletPrefab;
    public void setAnimationCurve(AnimationCurve curve)
    {
        changeCurve = curve;
    }
    //fix object to surface, and its step;
    public void fixToSurface()
    {
        Vector2 nextVertex = terrainManager.nextStepOnSurface(x);
        Vector2 prevVertex = terrainManager.prevStepOnSurface(x);
        Vector2 truePos = new Vector2();
        truePos.x = x;
        truePos.y = ((nextVertex.y - prevVertex.y) / (nextVertex.x - prevVertex.x)) * (x - nextVertex.x) + nextVertex.y;

        y = (int)truePos.y - offsetFromSurface;

        Vector2 angleVector = terrainManager.nextStepOnSurface(x) - (terrainManager.prevStepOnSurface(x));
        RotateBody(angleVector);
    }

    public void RotateBody(Vector2 vector)
    {
        float angle = MyUtils.makeRound(Mathf.Atan2(vector.y, vector.x));
        angle = Mathf.Rad2Deg * angle;
        if (vector.x < 0)
            tankGameObject.transform.rotation = new Quaternion(0, 0, angle - 360f, 0);
        else
            tankGameObject.transform.rotation = new Quaternion(0, 0, angle, 0);
    }
    private bool isTractionOK()
    {
        return false; //(_moveAngle > -_tractionAngle) || (_moveAngle < (-Mathf.PI + _tractionAngle));
    }

    public void MoveTank(Vector3 targetPoint,float duration) {
        if (!_moving) {
            _moving = true;
            setAnimationCurve(changeCurve);
            cr=StartCoroutine(MyUtils.Move(this.transform, targetPoint, duration));
        }
       
    }

    public void Move(bool forward)
    {
        Vector2 TankScrPos = terrainManager.camera.WorldToScreenPoint(endPoint.position);
        Vector2 TankScrPosS = terrainManager.camera.WorldToScreenPoint(startPoint.position);
        Vector3 tankPos = transform.position;
        // Vector2 target=terrainManager.nextStepOnSurface(TankScrPos.x);
        Vector2 target;
        if (forward)
            target = terrainManager.VertexHeights[terrainManager.upOnSurface2(TankScrPos.x)];
        else
            target = terrainManager.VertexHeights[terrainManager.upOnSurface2(TankScrPosS.x)+1];

        print(target);
        Vector3 targetWPos = terrainManager.camera.ScreenToWorldPoint(new Vector3(target.x,target.y,this.transform.position.z));

        float journeyLength = Vector3.Distance(tankPos, targetWPos);
        float distCovered = .5f;
        float fracJourney = distCovered / journeyLength;
        print("fracJourney" + fracJourney);
        Vector3 InterPolateTarget = Vector3.Lerp(tankPos, targetWPos, fracJourney);
         StartCoroutine(MyUtils.Move(this.transform,new Vector3(InterPolateTarget.x, InterPolateTarget.y, this.transform.position.z), Time.deltaTime));
    
    }

    private void OnEnd() {
        _moving = false;

    }
 
    bool touchup;
    public void Touch(bool forward) {
         cr = StartCoroutine(coRou(forward));
       
    }
    Coroutine cr;
    IEnumerator coRou(bool forward) {
        touchup = false;
        while (true)
        {
            yield return new WaitForEndOfFrame();
           // print("touch");
            Move(forward);
        }

    }
    public void DeTouch()
    {
        if(cr != null)
        StopCoroutine(cr);
        touchup = true;
        print("Detouch");
    }
    public void Pointer()
    {
      
        print("Pointer");
    }
    //bool touchinSc;
    Coroutine cr1;
    IEnumerator coRou1()
    {
        Vector3 mousePos = Input.mousePosition;
        Vector3 delta = Vector3.zero;
        Vector2 direction;
        Quaternion quaternion;
        float speedroat = 5;
        float theta = 0;
        while (true)
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - aim.transform.position;
            yield return new WaitForFixedUpdate();
            theta = (Mathf.Atan2(direction.y, direction.x)) * Mathf.Rad2Deg;
            quaternion = Quaternion.AngleAxis(theta, Vector3.forward);
            aim.transform.rotation = Quaternion.Slerp(aim.transform.rotation, quaternion, speedroat);
            Vector3 diff = mousePos - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            print(diff.sqrMagnitude);
            //if (direction.sqrMagnitude > 3  )
            //{
            //    aim.transform.localScale = new Vector3(aim.transform.localScale.x * 3, aim.transform.localScale.y, 1);
            //}
            //    aim.transform.localScale = new Vector3(Vector3.Slerp(aim.transform.localScale, new Vector3(diff,0,0), speedroat).x
            //        , aim.transform.localScale.y, aim.transform.localScale.z);
            //}


            //delta = Input.mousePosition - mousePos;
            //if (delta.magnitude >5) {
            //    if (delta.magnitude > 1) delta = delta.normalized;

            //    print(delta);
            //    aim.transform.rotation = Quaternion.Euler(new Vector3(0, 0, theta));
            //}

        }
    }
        public void DragP()
    {
        cr1 = StartCoroutine(coRou1());
        print("DragP");
    }
    public void UpP()
    {
        if (cr1 != null)
            StopCoroutine(cr1);
        print("DragP");
    }
    public void Shoot() {
        Bullet bul = Instantiate(bulletPrefab, gun.transform) as Bullet;
        bul._initialAngle = aim.transform.GetChild(1).transform.rotation.z;
        bul.startPoint = aim.transform;
        bul.Shoot();
    }
    /**forceMove: move tanks even if slope is too high*/
    public void moveOneStep(Vector2 pos, bool forceMove = false)
    {

        //checkFuel();
        if (_moving)
        {
            //trace("Already moving...");
            return;
        }

        // _moveTween = new Tween(this, _moveStepTime);
        // _moveTween.onComplete = onMoveTweenComplete;
        // _moveTween.onUpdate = updateRotation;
        //tashkhise jahate harkat:
        Vector2 vector = new Vector2(pos.x - x, pos.y - y);
        if (MyUtils.getRoundDistance(pos.x, pos.y, x, y) > _moveStep)
            vector.Normalize();

        _moveAngle = Mathf.Atan2(vector.y, vector.x);
        //_exaustParticleSystem.start();
        //Starling.juggler.delayCall(_exaustParticleSystem.stop, 0.5);

        print("moveOneStep ::: angle =" + _moveAngle + "| _tractionAngle =" + _tractionAngle + isTractionOK());

        if (forceMove || isTractionOK())
        {
            float newX = x + vector.x;
            //do not let tanks go off screen:
            newX = limitToScreenWidth(newX);
            float newY = y + vector.y;
            //trace("newX, newY =", newX, newY);
            //_moveTween.moveTo(newX, newY);
            // Starling.juggler.add(_moveTween);
            _moving = true;
        }
        else
        {
            //trace("Oh I Cannot go to dest: forceMove ", forceMove);
            onMoveTweenComplete();
        }
        //}
        // dispatchEvent(new TankEvent(TankEvent.POSITION_CHANGED));
        //} //if has fuel
    }
    private void onMoveTweenComplete()
    {
        _moving = false;
        if (_nextTargetPos != null)
        {
            if (Vector2.Distance(_nextTargetPos, new Vector2(x, y)) > _moveStep)
            {
                moveOneStep(CloneVec(_nextTargetPos), true);
                //trace("Still moving to: ", _nextTargetPos, "diff is", Point.distance(_nextTargetPos, new Point(x, y)));
            }
            else if (_targetPoints != null && _targetPoints.Count > 0)
            {
                walkThroughPath();
            }
            else
            {
                _nextTargetPos = new Vector2();
               // dispatchMoveComplete();
                //PlayOnline(_play).msgProcessed();
            }
        }
        else
        {
            /*if(_hintPowerBar.visible)
                updateHint();*/
            //trace("Tank move complete");
        }
    }
    private float limitToScreenWidth(float theX)
    {

        if (theX < terrainManager._STEP)

            theX = terrainManager._STEP;
        if (theX > Screen.width - terrainManager._STEP)
            theX = Screen.width - terrainManager._STEP;
        return theX;
    }
    private Vector2 CloneVec(Vector2 vec)
    {
        Vector2 vector = new Vector2();
        vector = vec;
        return vector;
    }
    private void walkThroughPath(bool forceMove = true) {
        _nextTargetPos = shiftVec(_targetPoints, 0);
        //trace("moving to: ", _nextTargetPos);
        moveOneStep(CloneVec(_nextTargetPos), forceMove);
    }
    protected void checkFuel()
    {
    //if(isThisPlayerTank())
    //    useFuel(_fuelUseEachMove);
    }
    private Vector2 shiftVec(List<Vector2> vecList,int indx) {
        Vector2 vec = vecList[indx];
        vecList.RemoveAt(indx);
        return vec;
    }
}

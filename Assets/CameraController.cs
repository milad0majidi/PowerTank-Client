﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private Camera camera;
    private float preferredOrthographicSize;
    private float maxOrthographicSize;
    private new Transform transform;
    public Transform target;
    // Use this for initialization
    void Start () {

        camera = Camera.main;
        transform = camera.transform;
        preferredOrthographicSize = Screen.height/10;
        print(Screen.width);
        
        maxOrthographicSize = preferredOrthographicSize * 2;
        // camera.orthographicSize = preferredOrthographicSize;
        
       
    }
    private void Update()
    {
        Vector3 screenPos = camera.WorldToScreenPoint(target.position);
        Debug.Log("target is " + screenPos.x + " pixels from the left");
    }

}

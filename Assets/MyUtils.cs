﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyUtils : MonoBehaviour
{

    public static int makeRound(float val)
    {

        return (int)Mathf.Round(val * 1000) / 1000;
    }

    public static float distanceToLine(Vector2 p, Vector2 p1, Vector2 p2)
    {
        float x = p.x;
        float y = p.y;
        float x1 = p1.x;
        float y1 = p1.y;
        float x2 = p2.x;
        float y2 = p2.y;

        float A = x - x1;
        float B = y - y1;
        float C = x2 - x1;
        float D = y2 - y1;

        float dot = A * C + B * D;
        float len_sq = C * C + D * D;
        float param = -1;
        //in case of 0 length line
        if (len_sq != 0)
            param = dot / len_sq;

        float xx, yy;

        if (param < 0)
        {
            xx = x1;
            yy = y1;
        }
        else if (param > 1)
        {
            xx = x2;
            yy = y2;
        }
        else
        {
            xx = x1 + param * C;
            yy = y1 + param * D;
        }

        float dx = x - xx;
        float dy = y - yy;
        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    public static float getRundAngle(float x1, float y1, float x2, float y2)
    {
        float dx = x2 - x1;
        float dy = y2 - y1;
        return MyUtils.makeRound(Mathf.Atan2(dy, dx));
    }
    public static float getRoundDistance(float x1, float y1, float x2, float y2)
    {
			return MyUtils.makeRound(Mathf.Sqrt(MyUtils.makeRound((x1 - x2) * (x1 - x2)) + MyUtils.makeRound((y1 - y2) * (y1 - y2))));
	}

    public static IEnumerator MoveItems(Transform[] items, Vector3[] targets, float duration, System.Action onEnd = null, bool smooth = false)
    {
        float t = Time.time;
        float dt = 0;

        Vector3[] starts = new Vector3[items.Length];
        for (int i = 0; i < items.Length; i++)
        {
            starts[i] = items[i].position;
        }

        if (smooth)
        {
            for (; dt < duration; dt = Time.time - t)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    items[i].position = Vector3.Lerp(starts[i], targets[i], Mathf.Sqrt(dt / duration));
                }
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            for (; dt < duration; dt = Time.time - t)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    items[i].position = Vector3.Lerp(starts[i], targets[i], (dt / duration));
                }
                yield return new WaitForEndOfFrame();
            }
        }

        for (int i = 0; i < items.Length; i++)
        {
            items[i].position = targets[i];
        }

        if (onEnd != null)
            onEnd();
    }

    public static IEnumerator MoveItem(Transform item, Vector3 target, float duration, System.Action onEnd = null, bool smooth = false, AnimationCurve changeCurve = null)
    {
        float t = Time.time;
        float dt = 0;
        float value = 1;
        if (changeCurve != null)
            value = changeCurve.Evaluate(t);
        Vector3 start = item.position;

        if (smooth)
        {
            for (; dt < duration; dt = Time.time - t)
            {
                item.position = Vector3.Lerp(start, target, Mathf.Sqrt(dt / duration));
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            for (; dt < duration; dt = Time.time - t)
            {

                value = changeCurve.Evaluate(dt/duration);
                print(value);
                item.position = Vector3.Lerp(start, target, value*(dt / duration));
                yield return new WaitForEndOfFrame();
            }
        }

        item.position = target;

        if (onEnd != null)
            onEnd();
    }
    public static IEnumerator Move(Transform item, Vector3 target, float duration)
    {
     
        float t = Time.time;
        float dt = 0;

        Vector3 start = item.position;

        for (; dt < duration; dt = Time.time - t)
        {

            item.position = Vector3.Lerp(start, target, (dt / duration));
            yield return new WaitForSeconds(Time.deltaTime);
        }

        item.position = target;

    
    }
    public static float roundSin(float x) {
			return makeRound(Mathf.Sin(x));
    }
    public static float roundCos(float x)
    {
        return makeRound(Mathf.Cos(x));
    }
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Itween))]

public class TransformManager : MonoBehaviour
{

    private Itween t;


	[Header("Position")]
    public Transform[] path;
    public float[] movementSpeed;
    public AnimationCurve[] movementCurve;

	[Header("Rotation")]
    public Transform[] angle;
    public float[] rotatSpeed;
    public AnimationCurve[] rotatCurve;

	[Header("Scale")]
    public Transform[] size;
    public float[] resizeSpeed;
    public AnimationCurve[] resizeCurve;

    // Use this for initialization
    void OnEnable()
    {
        if(t == null)
            t = GetComponent<Itween>();
    }

    public bool GetOnWork()
    {
        return t.enabled;
    }

    public void SetMovement(int index, bool deactive)
    {
        if(t==null)return;
        t.activate(deactive);
        t.refPositionTween.setDestination(path[index].position);
        t.refPositionTween.setSpeed(movementSpeed[index]);
        t.refPositionTween.setAnimationCurve(movementCurve[index]);
        t.refPositionTween.play();
    }

    public void SetMovement(int index)
    {
        t.activate(false);
        t.refPositionTween.setDestination(path[index].position);
        t.refPositionTween.setSpeed(movementSpeed[index]);
        t.refPositionTween.setAnimationCurve(movementCurve[index]);
        t.refPositionTween.play();
    }


    public void SetMovementFromTo(int indexFrom,int indexTo)
    {
        t.activate(false);
        t.transform.position = path[indexFrom].position;

        t.refPositionTween.setDestination(path[indexTo].position);
        t.refPositionTween.setSpeed(movementSpeed[indexTo]);
        t.refPositionTween.setAnimationCurve(movementCurve[indexTo]);
        t.refPositionTween.play();
    }

  //  public void SetRotate (int index) {
  //      t.activate(false);
  //      Vector2 tempV2 = new Vector2 (angle[index].eulerAngles.y,angle[index].eulerAngles.z);
  //      t.refRotationTween.setDestination (tempV2);
  //      t.refRotationTween.setSpeed (rotatSpeed[index]);
  //      t.refRotationTween.setAnimationCurve (rotatCurve[index]);
  //      t.refRotationTween.play ();
  //  }

    public void SetResize (int index) {
		
        t.activate(false);
		t.refScaleTween.setDestination (size[index].localScale);
        t.refScaleTween.setSpeed (resizeSpeed[index]);
        t.refScaleTween.setAnimationCurve (resizeCurve[index]);
        t.refScaleTween.play ();
    }
	public void SetResize (int index,bool deactive) {

		t.activate(deactive);
		t.refScaleTween.setDestination (size[index].localScale);
		t.refScaleTween.setSpeed (resizeSpeed[index]);
		t.refScaleTween.setAnimationCurve (resizeCurve[index]);
		t.refScaleTween.play ();
	}

    //void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.green;
    //    Vector3 tempV3;

    //    if (path != null)
    //        for (int i = 0; i < (path.Length - 1); i++)
    //        {
    //            Gizmos.DrawLine(path[i].position, path[i + 1].position);
    //        }

    //    Gizmos.color = Color.magenta;

    //    if (size != null)
    //        for (int i = 0; i < size.Length; i++)
    //        {
    //            tempV3 = size[i].position + new Vector3(0, size[i].localScale.y / 2, 0);
    //            Gizmos.DrawWireCube(tempV3, size[i].localScale);
    //        }


    //}
}

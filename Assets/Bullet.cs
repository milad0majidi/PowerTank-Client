﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{


    public float _initialVelocity = .1f;
    public float _initialAngle;
    protected float _g = 10;
    protected float _t = 0;
    int WIND_VELOCITY = 0;
    protected float X = 0;
    protected float Y = 0;
    protected float x = 0;
    protected float y = 0;
    [HideInInspector]
    public Transform startPoint;
    // Use this for initialization
    void Start()
    {

    }
    public void Shoot()
    {
        StartCoroutine(ShootTheBullet());
    }
    IEnumerator ShootTheBullet()
    {
        float time = 0;
        x = Camera.main.ScreenToWorldPoint(this.transform.position).x;
        y = Camera.main.ScreenToWorldPoint(this.transform.position).y;
        //startPoint.position = Camera.main.ScreenToWorldPoint(startPoint.position);

        //transform.Translate()
        while (true)
        {

            updateMovement(time);
            yield return new WaitForSeconds(.03f);
            time += .03f;//Time.deltaTime;
            print(Time.deltaTime);
        }
    }
    protected void updateMovement(float t)
    {
        //super.updateMovement();

        X = _initialVelocity * t * Mathf.Cos(_initialAngle);//MyUtils.makeRound(WIND_VELOCITY* .002f)+.1f;
        Y = MyUtils.makeRound(_initialVelocity * Mathf.Sin(_initialAngle)) - _g * MyUtils.makeRound(Mathf.Pow(t, 2));

        x += X;
        y += Y;
        transform.position = new Vector3(X + transform.position.x, Y + transform.position.y, 0);
        // transform.position=new Vector3(Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0)).x+ startPoint.position.x, Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0)).y+startPoint.position.y, 1);
        //updateRotation(transform);
       // transform.rotation = Quaternion.LookRotation(startPoint.position);
    }

    protected void updateRotation(Transform obj) {
			obj.rotation =new Quaternion(0,0, Mathf.Atan2(Y, X),0);
		}
}

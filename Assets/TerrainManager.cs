﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class TerrainManager : MonoBehaviour
{
    public enum Plane
    {
        Front,
        Top,
        Detail
    }
    public Plane PlaneType { get; set; }
    [HideInInspector]
    public Camera camera;

    //A reference to the created mesh object
    public GameObject MeshObject { get; set; }

    public Material MainMaterial;// { get; set; }
    public float MainMaterialXTiling; //{ get; set; }
    public float MainMaterialYTiling;//{ get; set; }
    public float MainMaterialRotation; //{ get; set; }

    public Material TopMaterial;//{ get; set; }
    public float TopMaterialXTiling { get; set; }
    public float TopMaterialYTiling { get; set; }
    public float TopMaterialRotation { get; set; }
    public bool DrawTopMeshCollider { get; set; }
    public bool DrawTopMeshRenderer { get; set; }

    public Material DetailMaterial; //{ get; set; }
    public float DetailMaterialXTiling { get; set; }
    public float DetailMaterialYTiling { get; set; }
    public float DetailMaterialRotation { get; set; }
    public bool DrawDetailMeshRenderer { get; set; }

    public float MainPlaneHeight;// { get; set; }
    public float TopPlaneHeight; //{ get; set; }
    public float DetailPlaneHeight; //{ get; set; }

    public bool MainPlaneFollowTerrainCurve; //{ get; set; }
    public bool DetailPlaneFollowTerrainCurve;//{ get; set; }

    public float CornerMeshWidth { get; set; }
    public Vector3 DetailPlaneOffset = new Vector3(0, .1f, -.2f);


    //Top verts
    public List<Vector3> KeyTopVerticies { get; set; }
    public List<Vector3> AllTopVerticies { get; set; }

    //Bottom verts
    public List<Vector3> KeyBottomVerticies { get; set; }
    public List<Vector3> AllBottomVerticies { get; set; }

    //All verts in mesh plane
    public List<Vector3> PlaneVerticies { get; set; }

    //All verts in plane (after rotation, if any)
    public List<Vector3> RotatedPlaneVerticies { get; set; }

    //Reference to the components we need to build our mesh
    private MeshFilter meshFilter { get; set; }
    private MeshRenderer meshRenderer { get; set; }
    private MeshCollider meshCollider { get; set; }
    public Mesh mesh { get; set; }

    //Determine the top and bottom y values that will be used for terrain generation
    public float MinimumKeyVertexHeight;
    public float MaximumKeyVertexHeight;
    public List<Vector2> VertexHeights;

    //The minimum and maximum values between key points in the terrain
    public float MinimumKeyVertexSpacing;
    public float MaximumKeyVertexSpacing;

    //How far apart do we space our calculated verticies?
    public float VertexSpacing;
    public float CalculatedVertexSpacing;
    public float SpacingScaleF;

    //How long is the mesh (provided it is not broken by hitting a new rule)
    float MeshLength;

    //Are the points generated along an angle (default is 0)
    public float Angle;

    //Track where we currently are, how far we have traveled overall, and how far since the beginning of this rule so we know when to 
    //break to the next rule
    public float CurrentLocation { get; set; }
    public Vector3 OriginalStartPoint;// { get; set; }

   // [HideInInspector]
    //public List<Vector2> verts = new List<Vector2>();
    [HideInInspector]
    public int _STEP;
    // Use this for initialization
    public Transform StartObj;
    public Vector3 GetScreenPosition(Vector3 target) {
        return camera.WorldToScreenPoint(target);
    }
    void Start()
    {
        camera = Camera.main;
        OriginalStartPoint = StartObj.position;
        //Generate the initial terrain to avoid slowdown once we start
        // VertexHeights = new List<float> { };
        List<float> VertexHeight = new List<float> { 10, 10, 100, 100, 100, 100, 100, 100, 100, 100, 200, 150, 100, 200, 250 , 150, 110, 180, 100, 150, 110, 180, 100
        ,100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 200, 150, 100, 200, 250 , 150, 110, 180, 100, 150, 110, 180, 100};
        
        VertexSpacing =Mathf.RoundToInt(Screen.width / VertexHeight.Count);
        _STEP = (int)VertexSpacing;
        Shader.WarmupAllShaders();
        PlaneType = Plane.Front;
        float xVert=0;
        for (int i = 0; i < VertexHeight.Count; i++)
        {
            VertexHeights.Add( new Vector2(xVert, VertexHeight[i]));
            xVert += _STEP;
        }
        // OriginalStartPoint = GetScreenPosition(OriginalStartPoint);
        Create(OriginalStartPoint, Angle, GenerateKeyVerticies(VertexHeights));

       
    }

    private List<float> GetVertHeights(List<Vector2> vertList)
    {
        List<float> vertHeight = new List<float>();
        foreach (Vector2 height in vertList)
        {
            vertHeight.Add(height.y);
        }
        return vertHeight;
    }

    public void GenerateTerrain(List<Vector2> vertList)
    {
        Create(OriginalStartPoint, Angle, GenerateKeyVerticies(vertList));
    }


    public List<Vector3> GenerateKeyVerticies(List<Vector2> vertexHeights)
    {
        
        List<Vector3> verticies = new List<Vector3>();
        CurrentLocation = OriginalStartPoint.x;
        //Determine where we will start and end vertex generation
        float startLocation = CurrentLocation;
        MeshLength = vertexHeights.Count * VertexSpacing;
        float endLocation = MeshLength + startLocation;
        //verts.Clear();
        //Loop until we've generated enough vertices to reach the desired mesh length (or we hit a new rule)
        for (int i = 0; i < vertexHeights.Count; i++)
        {
            float y=camera.ScreenToWorldPoint(new Vector3(CurrentLocation, vertexHeights[i].y, 0)).y;
            float x= camera.ScreenToWorldPoint(new Vector3(CurrentLocation, vertexHeights[i].y, 0)).x;
            // float y = vertexHeights[i];
            // float x = CurrentLocation;
            verticies.Add(new Vector3(x, y, OriginalStartPoint.z));
            //Update our current location 
            //verts.Add(new Vector2(x, y));
            
            CurrentLocation += VertexSpacing;
        }

        return verticies;
    }

    //Get a point x distance below the lowest y vertex in the set of verticies
    public List<Vector3> GenerateFlatLowerBoundVerticies(List<Vector3> topVerticies, float distanceBelowLowestYVert)
    {
        float minY = topVerticies.Select(t => t.y).Min();
        float bottomY = minY - distanceBelowLowestYVert;

        List<Vector3> lowerVerticies = new List<Vector3>();
        for (int i = 0; i < topVerticies.Count; i++)
        {
            Vector3 topVertex = topVerticies[i];
            lowerVerticies.Add(new Vector3(topVertex.x, bottomY, topVertex.z));
        }
        return lowerVerticies;
    }

    //Add the verts in a consistent way so I know the triangle generation will be the same
    public List<Vector3> GetPlaneVerticies(List<Vector3> lowerVerticies, List<Vector3> topVerticies)
    {
        List<Vector3> planeVerticies = new List<Vector3>();
        for (int i = 0; i < topVerticies.Count; i++)
        {
            planeVerticies.Add(lowerVerticies[i]);
            planeVerticies.Add(topVerticies[i]);
        }
        return planeVerticies;
    }


    public List<Vector3> GenerateCalculatedVertices(List<Vector3> keyVertices)
    {

        List<Vector3> allVerticies = new List<Vector3>();

        //Start by inserting after the first key point
        int insertIndex = 1;

        //set CalculatedVertexSpacing
        CalculatedVertexSpacing = VertexSpacing * SpacingScaleF;

        for (int i = 0; i < keyVertices.Count - 1; i++)
        {

            //Add the key vert
            allVerticies.Add(keyVertices[i]);

            Vector3 currentVertex = keyVertices[i];
            Vector3 nextVertex = keyVertices[i + 1];

            float x0 = currentVertex.x;
            float x1 = nextVertex.x;

            float y0 = currentVertex.y;
            float y1 = nextVertex.y;


            //How many segments between our two key points
            int totalSegments = Mathf.CeilToInt(Mathf.Ceil(x1 - x0) / CalculatedVertexSpacing);

            //The width of each of these segments		
            float segmentWidth = (x1 - x0) / totalSegments;


            for (int j = 1; j < totalSegments; j++)
            {
                float newX = x0 + j * segmentWidth;

                //Calculate our new y value by cosine interpolation
                float mu = (float)j / (float)totalSegments;
                float newY = CosineInterpolate(y0, y1, mu);

                Vector3 newVert = new Vector3(newX, newY, OriginalStartPoint.z);
                allVerticies.Insert(insertIndex, newVert);

                //Move to the next calculated point
                insertIndex += 1;
            }

            //Jump over the key point and move on to the next calculated point
            insertIndex += 1;

            if (i == keyVertices.Count - 2)
            {
                allVerticies.Add(nextVertex);
            }
        }

        return allVerticies;
    }
    //Interpolate the y values based off of the cosine function
    public float CosineInterpolate(float y1, float y2, float mu)
    {
        //Mu is between 0 and 1 - it is the relative position between the two y values
        float mu2;

        mu2 = (1 - Mathf.Cos(mu * Mathf.PI)) / 2;
        return (y1 * (1 - mu2) + y2 * mu2);
    }

    public void Create(Vector3 origin, float angle, List<Vector3> keyVerticies)
    {
        //Update all our verticies
        SetVerticies(origin, angle, keyVerticies);
        CreateMesh();
    }

    private void CreateMesh()
    {
        if (mesh == null)
        {
            InstantiateMeshObject();
        }
        else
        {
            mesh.Clear();
            AddMeshComponents();
        }
        //for (int i = 0; i < RotatedPlaneVerticies.Count; i++)
        //{
        //    print(RotatedPlaneVerticies[i]);
        //}
       
        //Set verts, uvs and triangles for the mesh from the rotated plane verts (these are populated even if there was no rotation)
        mesh.vertices = RotatedPlaneVerticies.ToArray();
        mesh.triangles = GetMeshTriangles(RotatedPlaneVerticies).ToArray();
        mesh.uv = GetUVMapping(RotatedPlaneVerticies).ToArray();

        if (PlaneType == Plane.Front && MainMaterial != null)
        {
            meshRenderer.GetComponent<Renderer>().sharedMaterial = MainMaterial;
            // meshRenderer.GetComponent<Renderer>().sharedMaterial.renderQueue = RenderQueue.FrontPlane;

        }
        if (PlaneType == Plane.Detail && DetailMaterial != null)
        {
            meshRenderer.GetComponent<Renderer>().sharedMaterial = DetailMaterial;
            // meshRenderer.GetComponent<Renderer>().sharedMaterial.renderQueue = RenderQueue.DetailPlane;

        }
        if (PlaneType == Plane.Top && DrawTopMeshRenderer && TopMaterial != null)
        {
            meshRenderer.GetComponent<Renderer>().sharedMaterial = TopMaterial;
        }


        //Add collider to the top plane
        // AddCollider();

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }
    private List<int> GetMeshTriangles(List<Vector3> planeVerticies)
    {
        //Assume the mesh is a single plane
        List<int> triangles = new List<int>();
        for (int i = 0; i < planeVerticies.Count; i += 2)
        {

            //Don't worry about figuring this all out for now, just break if we're past the max index for the vertices
            if ((i + 3) > planeVerticies.Count)
            {
                break;
            }

            triangles.Add(i);
            triangles.Add(i + 1);
            triangles.Add(i + 3);

            triangles.Add(i + 3);
            triangles.Add(i + 2);
            triangles.Add(i);

        }
        return triangles;
    }
    private List<Vector2> GetUVMapping(List<Vector3> planeVerticies)
    {

        // TransformHelpers th = new TransformHelpers();

        float textureHeight = 1;
        float textureWidth = 1;


        float xTiling = 1f;
        float yTiling = 1f;

        //The uv tiling also has to factor in the height and width of the textures we are using
        if (MainMaterial != null && MainMaterial.mainTexture != null)
        {
            textureHeight = MainMaterial.mainTexture.height;
            textureWidth = MainMaterial.mainTexture.width;
        }

        if (TopMaterial != null && TopMaterial.mainTexture != null)
        {
            textureHeight = TopMaterial.mainTexture.height;
            textureWidth = TopMaterial.mainTexture.width;
        }

        if (DetailMaterial != null && DetailMaterial.mainTexture != null)
        {
            textureHeight = DetailMaterial.mainTexture.height;
            textureWidth = DetailMaterial.mainTexture.width;
        }

        //Set our tiling depending on the plane
        if (PlaneType == Plane.Front)
        {
            xTiling = MainMaterialXTiling;
            yTiling = MainMaterialYTiling;
        }

        if (PlaneType == Plane.Top)
        {
            xTiling = TopMaterialXTiling;
            yTiling = TopMaterialYTiling;
        }

        if (PlaneType == Plane.Detail)
        {
            xTiling = DetailMaterialXTiling;
            yTiling = DetailMaterialYTiling;
        }


        //Tile the texture as needed
        textureWidth = textureWidth / xTiling;
        textureHeight = textureHeight / yTiling;

        //Track how far along we are on the top texture mapping
        float currentTopTextureX = 0;


        List<Vector2> uvs = new List<Vector2>();
        for (int i = 0; i < planeVerticies.Count; i++)
        {
            Vector3 vertex = planeVerticies[i];
            Vector3 previousVertex = Vector3.zero;
            if (i > 0)
            {
                previousVertex = planeVerticies[i - 1];
            }


            //Our standard uv mapping is just our point in space divided by the width and the height of our texture (assuming an x/y plane)
            float xMapping = vertex.x / textureWidth;
            float yMapping = vertex.y / textureHeight;


            if (PlaneType == Plane.Top)
            {
                //We have to factor in the rise in y (from the lowest point in the list to our current point), as well as the x movement across
                //in our uv mapping.  This is because this is not a flat plane

                //The first time through, set our current x position based off the vertex
                if (currentTopTextureX == 0) { currentTopTextureX = vertex.x; }
                xMapping = (currentTopTextureX) / textureWidth;

                //After that, increment it by the length of the line between the two points (which includes the movement in the x and y plane) in the uv mapping
                if (previousVertex != Vector3.zero)
                {
                    float length = GetLengthOfLine(vertex, previousVertex);
                    currentTopTextureX += length;
                    xMapping = (currentTopTextureX) / textureWidth;
                }

                //For the y mapping, since we are in the z plane divide the texture height by z instead of y.
                yMapping = vertex.z / textureHeight;
            }


            //If we want the uv mapping to follow the curve of the plane instead, set it here
            if (PlaneType == Plane.Front && MainPlaneFollowTerrainCurve)
            {
                if (i % 2 == 0)
                {
                    yMapping = -MainPlaneHeight / textureHeight;
                }
                else
                {
                    yMapping = 1;
                }
            }

            if (PlaneType == Plane.Detail && DetailPlaneFollowTerrainCurve)
            {
                if (i % 2 == 0)
                {
                    yMapping = -DetailPlaneHeight / textureHeight;
                }
                else
                {
                    yMapping = 1;
                }
            }

            //Finally set the actual uv mapping
            Vector2 uv = new Vector2(xMapping, yMapping);


            //Now set the rotation of the uv mapping
            if (MainMaterialRotation != 0 && PlaneType == Plane.Front)
            {
                uv = RotateVertex(uv, MainMaterialRotation);
            }

            if (DetailMaterialRotation != 0 && PlaneType == Plane.Detail)
            {
                uv = RotateVertex(uv, DetailMaterialRotation);
            }


            if (TopMaterialRotation != 0 && PlaneType == Plane.Top)
            {
                uv = RotateVertex(uv, TopMaterialRotation);
            }

            uvs.Add(uv);
        }

        return uvs;

    }

    //Rotate a vertex by a given angle
    public Vector3 RotateVertex(Vector3 vertex, float angle)
    {
        if (angle == 0)
        {
            return vertex;
        }

        var rotation = Quaternion.Euler(0, 0, angle);
        return rotation * vertex;
    }

    public float GetLengthOfLine(Vector2 lineStart, Vector2 lineEnd)
    {
        float x = Mathf.Abs(lineStart.x - lineEnd.x);
        float y = Mathf.Abs(lineStart.y - lineEnd.y);
        x = Mathf.Pow(x, 2);
        y = Mathf.Pow(y, 2);
        return Mathf.Sqrt(x + y);
    }

    public void CreateCorner(List<Vector3> keyTopVerticies, List<Vector3> keyBottomVerticies)
    {
        //Update all our verticies
        SetCornerVerticies(keyTopVerticies, keyBottomVerticies);
        CreateMesh();
    }
    private void SetCornerVerticies(List<Vector3> keyTopVerticies, List<Vector3> keyBottomVerticies)
    {
        KeyTopVerticies = keyTopVerticies;
        KeyBottomVerticies = keyBottomVerticies;

        AllTopVerticies = keyTopVerticies;
        AllBottomVerticies = keyBottomVerticies;

        PlaneVerticies = GetPlaneVerticies(AllBottomVerticies, AllTopVerticies);
        RotatedPlaneVerticies = PlaneVerticies;

    }

    private void SetVerticies(Vector3 origin, float angle, List<Vector3> keyVerticies)
    {
        if (keyVerticies != null) { KeyTopVerticies = keyVerticies; }



        Settings th = new Settings();

        AllTopVerticies = GenerateCalculatedVertices(KeyTopVerticies);

        //For the front plane, set a fixed lower boundary on the verts (bottom of mesh will be a straight line)
        if (PlaneType == Plane.Front)
        {
            Vector3 firstBottomVertex = AllTopVerticies[0];
            Vector3 shift = new Vector3(firstBottomVertex.x, firstBottomVertex.y - MainPlaneHeight, firstBottomVertex.z);

            AllBottomVerticies = th.MoveStartVertex(AllTopVerticies, AllTopVerticies[0], shift, true);
            KeyBottomVerticies = th.MoveStartVertex(KeyTopVerticies, AllTopVerticies[0], shift, true);

        }

        if (PlaneType == Plane.Detail)
        {
            Vector3 firstBottomVertex = th.CopyVertex(AllTopVerticies[0]);
            Vector3 shift = new Vector3(firstBottomVertex.x, firstBottomVertex.y - DetailPlaneHeight, firstBottomVertex.z);

            AllBottomVerticies = th.MoveStartVertex(AllTopVerticies, firstBottomVertex, shift, true);
            KeyBottomVerticies = th.MoveStartVertex(KeyTopVerticies, firstBottomVertex, shift, true);
        }

        //For the top of the mesh, shift the verticies in the z direction
        if (PlaneType == Plane.Top)
        {

            //The bottom verts are a copy of the top
            AllBottomVerticies = th.CopyList(AllTopVerticies);
            KeyBottomVerticies = th.CopyList(KeyTopVerticies);

            Vector3 firstBottomVertex = AllTopVerticies[0];

            //Then shift the top verts into the z plane
            AllTopVerticies = th.MoveStartVertex(AllTopVerticies, AllTopVerticies[0], new Vector3(firstBottomVertex.x, firstBottomVertex.y, firstBottomVertex.z + TopPlaneHeight), false);
        }


        //Now swtich top and bottom verticies together into a plane
        PlaneVerticies = GetPlaneVerticies(AllBottomVerticies, AllTopVerticies);


        //For the top plane we have to move our point of origin based on the plane height
        if (PlaneType == Plane.Top)
        {
            origin = new Vector3(origin.x, origin.y, origin.z + TopPlaneHeight);
        }


        //Now move the whole plane to the point of origin (usually where the last mesh ended)
        //Move relative to the vertex at index 1 (the top of the plane) instead of zero (the bottom of the plane) since we want to match the top of the meshes
        PlaneVerticies = th.MoveStartVertex(PlaneVerticies, PlaneVerticies[1], origin, false);

        //Store the rotated verticies so we know where the actual end point of the mesh is (and where to start the next one)
        //Create the mesh from the rotate verticies, but generate it from the non-rotated ones
        if (angle != 0)
        {
            RotatedPlaneVerticies = th.RotateVertices(PlaneVerticies, angle);
            RotatedPlaneVerticies = th.MoveStartVertex(RotatedPlaneVerticies, RotatedPlaneVerticies[1], origin, false);
        }
        else
        {
            RotatedPlaneVerticies = PlaneVerticies;
        }


    }


    private void InstantiateMeshObject()
    {
        MeshObject = new GameObject("MeshPiece");
        MeshObject.transform.SetParent(this.transform);
        AddMeshComponents();
    }
    private void AddMeshComponents()
    {

        if (meshFilter == null) { meshFilter = MeshObject.AddComponent<MeshFilter>(); }

        if (PlaneType == Plane.Top)
        {
            if (DrawTopMeshRenderer)
            {
                if (meshRenderer == null) { meshRenderer = MeshObject.AddComponent<MeshRenderer>(); }
            }
            if (DrawTopMeshCollider)
            {
                if (meshCollider == null) { meshCollider = MeshObject.AddComponent<MeshCollider>(); }
            }
        }
        else
        {
            if (meshRenderer == null) { meshRenderer = MeshObject.AddComponent<MeshRenderer>(); }
            if (meshCollider == null) { meshCollider = MeshObject.AddComponent<MeshCollider>(); }
        }

        GameObject.DestroyImmediate(meshFilter.sharedMesh);
        meshFilter.sharedMesh = new Mesh();
        mesh = meshFilter.sharedMesh;

        // mesh = meshFilter.mesh;
    }

  

    public Vector2 nextStepOnSurface(float x)
    {
        int resX = (int)Mathf.Floor(x / _STEP);
        return VertexHeights[1 + resX]; //next step; 	
    }

    public Vector2 prevStepOnSurface(float x)
    {
        int resX = (int)Mathf.Ceil(x / _STEP);
        int indx = -1 + resX >= 0 ? -1 + resX : VertexHeights.Count - 1 + resX;
        return VertexHeights[indx]; //prev step; 
    }

    public Vector2 upOnSurface(float x)
    {
        int resX = (int)Mathf.Round(x / _STEP);
        int indx = resX >= 0 ? resX : VertexHeights.Count + resX;
        return VertexHeights[indx];
    }
    public int upOnSurface2(float x)
    {
        int resX = (int)Mathf.Round(x / _STEP);
        int indx = resX >= 0 ? resX : VertexHeights.Count + resX;
        return indx;
    }
    public int upOnSurface3(float x)
    {
        int resX = (int)Mathf.Round(x / _STEP);
        int indx = resX >= 0 ? resX : VertexHeights.Count - resX;
        return indx;
    }
    public Vector2[] getNearestSegment(Vector2 pos)
    {
        Vector2[] tmp = new Vector2[2];
        Vector2 nearVertex = upOnSurface(pos.x);
        Vector2 prevVertex = prevStepOnSurface(nearVertex.x);
        Vector2 prePrevVertex = prevStepOnSurface(prevVertex.x);
        Vector2 nextVertex = nextStepOnSurface(nearVertex.x);
        Vector2 postNextVertex = nextStepOnSurface(nextVertex.x);

        // find the nearest line segment to the bullet pos
        float dist1 = MyUtils.makeRound(MyUtils.distanceToLine(pos, nearVertex, prevVertex));
        float dist2 = MyUtils.makeRound(MyUtils.distanceToLine(pos, nearVertex, nextVertex));
        float dist3 = MyUtils.makeRound(MyUtils.distanceToLine(pos, prevVertex, prePrevVertex));
        float dist4 = MyUtils.makeRound(MyUtils.distanceToLine(pos, nextVertex, postNextVertex));
        Vector2 firstVertex = new Vector2();
        Vector2 secondVertex = new Vector2();

        if (dist1 <= dist2 && dist1 <= dist3 && dist1 <= dist4)
        {
            firstVertex = nearVertex;
            secondVertex = prevVertex;
        }
        else if (dist2 <= dist1 && dist2 <= dist3 && dist2 <= dist4)
        {
            firstVertex = nearVertex;
            secondVertex = nextVertex;
        }
        else if (dist3 <= dist1 && dist3 <= dist2 && dist3 <= dist4)
        {
            firstVertex = prevVertex;
            secondVertex = prePrevVertex;
        }
        else if (dist4 <= dist1 && dist4 <= dist2 && dist4 <= dist3)
        {
            firstVertex = nextVertex;
            secondVertex = postNextVertex;
        }
        tmp[0] = firstVertex;
        tmp[1] = secondVertex;
        return tmp;
    }
    public float perpendecularAngle(Vector2 pos)
    {
        float perpAngle = surfaceAngle(pos) + Mathf.PI * .5f;

        if (perpAngle < 0)
            perpAngle += MyUtils.makeRound(Mathf.PI * 2);
        else if (perpAngle > Mathf.PI * 2)
            perpAngle -= MyUtils.makeRound(Mathf.PI * 2);
        return perpAngle;
    }
    public float surfaceAngle(Vector2 pos)
    {
        Vector2[] nearVertices = getNearestSegment(pos);
        Vector2 firstVertex = nearVertices[0];
        Vector2 secondVertex = nearVertices[1];
        float surfaceAngle = MyUtils.makeRound(MyUtils.getRundAngle(firstVertex.x, firstVertex.y, secondVertex.x, secondVertex.y));
        return surfaceAngle;
    }
}

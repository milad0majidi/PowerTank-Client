﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Settings  {

    //Rotate a list of verticies by a given angle
    public List<Vector3> RotateVertices(List<Vector3> verticies, float angle)
    {
        List<Vector3> rotated = new List<Vector3>();
        for (int i = 0; i < verticies.Count(); i++)
        {
            Vector3 vertex = verticies[i];
            rotated.Add(RotateVertex(vertex, angle));
        }

        return rotated;
    }
    //Rotate a vertex by a given angle
    public Vector3 RotateVertex(Vector3 vertex, float angle)
    {
        if (angle == 0)
        {
            return vertex;
        }

        var rotation = Quaternion.Euler(0, 0, angle);
        return rotation * vertex;
    }
    public Vector3 CopyVertex(Vector3 temp)
    {
        return new Vector3(temp.x, temp.y, temp.z);
    }
    public List<Vector3> CopyList(List<Vector3> verticies)
    {
        List<Vector3> copy = new List<Vector3>();
        for (int i = 0; i < verticies.Count; i++)
        {
            copy.Add(new Vector3(verticies[i].x, verticies[i].y, verticies[i].z));
        }
        return copy;
    }

    public List<Vector3> MoveStartVertex(List<Vector3> verticies, Vector3 newStartVertex, bool copyVertices, TerrainManager.Plane planeType)
    {
        int startIndex = 0;
        if (planeType == TerrainManager.Plane.Front || planeType == TerrainManager.Plane.Detail)
        {
            startIndex = 1;
        }

        Vector3 originalStartVertex = verticies[startIndex];
        return MoveStartVertex(verticies, originalStartVertex, newStartVertex, copyVertices);
    }
    //Move a list of verticies to a new start location.  Keep all their positions relative to each other
    public List<Vector3> MoveStartVertex(List<Vector3> verticies, Vector3 originalStartVertex, Vector3 newStartVertex, bool copyVertices)
    {
        List<Vector3> copy = new List<Vector3>();

        //And how much we have to move by
        Vector3 moveVector = newStartVertex - originalStartVertex;

        //Now move every vector in our list proportial to our new location
        for (int i = 0; i < verticies.Count; i++)
        {
            Vector3 currentVertex = verticies[i];

            if (copyVertices)
            {
                Vector3 newVertex = new Vector3(currentVertex.x, currentVertex.y, currentVertex.z);
                copy.Add(newVertex + moveVector);
            }
            else
            {
                verticies[i] = verticies[i] + moveVector;
            }
        }

        if (copyVertices)
        {
            return copy;
        }
        else
        {
            return verticies;
        }

    }
}

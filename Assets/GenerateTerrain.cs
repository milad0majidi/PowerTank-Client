﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateTerrain : MonoBehaviour {
    [Range(.0f, 20f)]
    public float heightScale=5f;
    [Range(.0f, 40f)]
    public float detailScale = 5f;
    Mesh mesh;
    Vector3[] vertices;
    public MeshFilter meshFilter;

   [ContextMenu("gen")]
    void GenerateWaves() {
        mesh = meshFilter.mesh;
        vertices = mesh.vertices;
        int counter = 0;
        int yLevel = 0;
        for (int i = 0; i < 11; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                CalculationMeth(counter,yLevel);
                    counter++;
            }
            yLevel++;
        }
        mesh.vertices = vertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        Destroy(gameObject.GetComponent<MeshCollider>());
        MeshCollider coll = gameObject.AddComponent<MeshCollider>();
        coll.sharedMesh = null;
        coll.sharedMesh = mesh;
    }
    public bool waves;
    void CalculationMeth(int i, int j)
    {
        vertices[i].z = Mathf.PerlinNoise((vertices[i].x + transform.position.x) / detailScale,
            (vertices[i].y + transform.position.y) / detailScale) * heightScale;
        vertices[i].z -= j;
    }    // Use this for initialization
    void Start () {
         GenerateWaves();

        }

        // Update is called once per frame
        void Update () {
		
	}
}

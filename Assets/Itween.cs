﻿using UnityEngine;
using System.Collections;

public class Itween : MonoBehaviour
{
    public PositionTween    refPositionTween;
    public RotationTween    refRotationTween;
    public ScaleTween       refScaleTween;

    private bool work = false;
    private bool deactive = false;

    void Awake()
    {
        refPositionTween = new PositionTween(transform);
        refRotationTween 	= new RotationTween(transform);
        refScaleTween 		= new ScaleTween(transform);
    }

    IEnumerator update()
    {

        work = true;
        yield return new WaitForEndOfFrame();

		while (refScaleTween.Update() || refPositionTween.Update() || refRotationTween.Update())
        {
            yield return new WaitForEndOfFrame();
        }

        if (deactive)
            gameObject.SetActive(false);

        work = false;
    }

    public void activate(bool value)
    {
        //enabled = true;
        deactive = value;

        if (!gameObject.activeSelf)
            gameObject.SetActive(true);

        if (!work)
        {
            StartCoroutine(update());
        }

    }

    //void OnDrawGizmos()
    //{
    //    if (Application.isPlaying)
    //    {
    //        refPositionTween.OnDrawGizmos();
    //        refScaleTween.OnDrawGizmos ();
    //    }
    //}

}

public class TransformItween
{

    protected bool autoChange = false;
    protected Vector2 from = Vector2.zero;
    protected Vector2 to = Vector2.zero;
    protected float changeSpeed = 0.0f;
    protected AnimationCurve changeCurve;

    protected float timer = 0.0f;
    protected bool fix = false;

    protected Transform refTransform;

    public bool Update()
    {
        if (autoChange)
            timer = Manager(timer);

        return autoChange;
    }

    protected virtual float Manager(float time) { return time; }

    public void play()
    {
        timer = 0;
        fix = false;
        autoChange = true;
    }

    public void setDestination(Vector2 pos)
    {
        to = pos;
    }

    public void setAnimationCurve(AnimationCurve curve)
    {
        changeCurve = curve;
    }

    public void setSpeed(float speed)
    {
        changeSpeed = speed;
    }
}

//Change Position
public class PositionTween : TransformItween
{

    public PositionTween(Transform trans)
    {
        refTransform = trans;
    }

    protected override float Manager(float time)
    {

        time += Time.deltaTime / changeSpeed;
        float Value = changeCurve.Evaluate(time);
        var originalZ = refTransform.position.z;
        if (!fix)
        {
            from = new Vector3(refTransform.position.x, refTransform.position.y,originalZ);
            fix = true;
        }

        float diffulteX = to.x - from.x;
        float diffulteY = to.y - from.y;
        refTransform.position = new Vector3(from.x + (Value * diffulteX), from.y + (Value * diffulteY),originalZ);

        if (Value == 1)
            autoChange = false;

        return time;
    }

    //public void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawLine(from, to);
    //}

}

//Change Rotation
public class RotationTween : TransformItween
{

    public RotationTween(Transform trans)
    {
        refTransform = trans;
    }

    protected override float Manager(float time)
    {

        time += Time.deltaTime / changeSpeed;
        float Value = changeCurve.Evaluate(time);

        if (!fix)
        {
            from = new Vector2(refTransform.eulerAngles.y, refTransform.eulerAngles.z);
            fix = true;
        }

        float diffulteX = to.x - from.x;
        float diffulteY = to.y - from.y;
        refTransform.eulerAngles = new Vector3(0, from.x + (Value * diffulteX), from.y + (Value * diffulteY));

        if (Value == 1)
            autoChange = false;

        return time;
    }

}

//Change Scale
public class ScaleTween : TransformItween
{

    public ScaleTween(Transform trans)
    {
        refTransform = trans;
    }

    protected override float Manager(float time)
    {

        time += Time.deltaTime / changeSpeed;
        float Value = changeCurve.Evaluate(time);

        if (!fix)
        {
            from = new Vector2(refTransform.localScale.x, refTransform.localScale.y);
            fix = true;
        }

        float diffulteX = to.x - from.x;
        float diffulteY = to.y - from.y;
        refTransform.localScale = new Vector2(from.x + (Value * diffulteX), from.y + (Value * diffulteY));

        if (Value == 1)
            autoChange = false;

        return time;
    }

    //public void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.yellow;
    //    Vector3 tempV3 = refTransform.position + new Vector3(0, refTransform.localScale.y / 2, 0);

    //    Gizmos.DrawWireCube(tempV3, refTransform.localScale);

    //    Gizmos.color = Color.red;
    //    tempV3 = refTransform.position + new Vector3(0, to.y / 2, 0);
    //    Gizmos.DrawWireCube(tempV3, to);

    //}

}


